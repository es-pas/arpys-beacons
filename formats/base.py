from abc import ABC, abstractmethod
from dataclasses import dataclass

from datetime import datetime

@dataclass
class BeaconData:
    """BeaconData декодированные данные маяка"""
    date: str
    latitude: float
    longitude: float
    behaviors: list
    is_new: bool

    # def __init__(self, task_uuid: str, driver: str, action: Action) -> None:
    #     self.task_uuid = task_uuid
    #     self.driver = driver
    #     self.action = action

class Beacon(ABC):
    """Beacon интерфейс маяка"""
    @abstractmethod
    def decode(self, message: str, getting_date: datetime = datetime.now()) -> BeaconData:
        """decode декодирует сообщение"""

class BeaconFactory(ABC):
    """BeaconFactory интерфейс фабрики маяка"""
    @abstractmethod
    def get_beacon(self, hex_id: str) -> Beacon:
        """get_beacon возвращает Beacon"""
