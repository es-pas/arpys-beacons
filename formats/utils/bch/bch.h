void decode_bch(int, int, int, int recd[], int alpha_to[], int index_of[], int*);
void read_p(int*, int*, int p[]);
void generate_gf(int*, int*, int p[], int alpha_to[], int index_of[]);
void encode_bch(int, int, int data[], int g[], int bb[]);
void gen_poly(int, int, int, int*, int*, int*, int p[], int alpha_to[], int index_of[], int g[]);