from  ..base import FormatterFactory

from .sisma_ag import SismaAgFactory
from .sisma_hf import SismaHfFactory
from .sisma_10rt import Sisma10RtFactory
from .sisma_bf import SismaBfFactory



QUASAR_04 = "quasar_04"
QUASAR_10 = "quasar_10"
QUASAR_30 = "quasar_30"
QUASAR_60 = "quasar_60"

def create_beacon_factory(format: str) -> BeaconFactory:
    # create_beacon_factory создает новый BeaconFactory выбранного format
    if name == SISMA_AG:
        return SismaAgFactory()
    elif name == SISMA_HF:
        return SismaHfFactory()
    elif name == SISMA_10RT:
        return Sisma10RtFactory()
    elif name == SISMA_BF:
        return SismaBfFactory()
    elif name == QUASAR_04:
        return Quasar04Factory()
    elif name == QUASAR_10:
        return Quasar10Factory()
    elif name == QUASAR_30:
        return Quasar30Factory()
    elif name == QUASAR_60:
        return Quasar60Factory()

    else:
        return None
