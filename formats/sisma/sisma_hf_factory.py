from ctypes import *
from datetime import datetime

from formats.sisma.sisma_hf import SismaHF

class SismaHfFactory(FormatterFactory):
    def __init__(self, bch_path: str):
        self.bch_path = bch_path
        
    def get_formatter(self, id: int, date: datetime) -> Formatter:
        return SismaHF(self.bch_path, id, date)
