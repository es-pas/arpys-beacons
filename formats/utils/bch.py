from ctypes import *
from datetime import datetime, timedelta

MAX_CORRECTABLE_ERRORS = 6
CODE_LENGTH = 85
DATA_BITS_LENGTH = 43
BCH_BITS_LENGTH = 42


self.mydll = CDLL(path)
self.func_init()

def calculate_bch(data_bits: str, bch_bits: str) -> :
    if len(data_bits) != DATA_BITS_LENGTH:
		return #"", false, errors.New("bch dataBits wrong length")

    # формирование бинарного массива из перевернутых данных
	reversed_data_bits := data_bits[::-1]

    self.inv_BCH = (bits[-42:])[::-1]
    self.inv_data = data_bits[::-1] + ("0" * 42)
    
    m = c_int(7)  #кол-во возможных ошибок
    n = c_int(0)

    mp = pointer(m)
    np = pointer(n)
    
    arr = c_int * 19
    p = arr()
    self.read_p(mp, np, p)

    m = mp.contents
    n = np.contents
    arr = c_int * 274287
    alpha_to = arr()
    index_of = arr()
    self.generate_gf(mp, np, p, alpha_to, index_of)

    length1 = c_int(85)
    t = c_int(6)
    k = c_int(43)
    g = arr()
    tp = pointer(t)
    dp = pointer(c_int(0))
    kp = pointer(k)
    self.gen_poly(m, n, length1, tp, dp, kp, p, alpha_to, index_of, g)

    bb = arr()
    arr = c_int * 524287
    data = arr()
    for i in range(43):
        data[i] = int(self.inv_data[i])
    self.encode_bch(length1, k, data, g, bb)
    
    encoder = ""
    for i in range(length1.value - k.value):
        encoder += str(bb[i])
    if self.inv_BCH == encoder:
        self.info["errors"] = "no"
    else:
        self.info["errors"] = "fixed"

    arr = c_int * 524287
    recd = arr()
    for i in range(length1.value - k.value):
        recd[i] = int(self.inv_BCH[i])
    for i in range(k.value):
        recd[i + length1.value - k.value] = data[i]

    ResultDecode = c_int(0)
    ptrResult = pointer(ResultDecode)

    self.decode_bch(t, length1, n, recd, alpha_to, index_of, ptrResult)

    S_DataDecText = ""
    for i in range(k.value - 1, length1.value):
        S_DataDecText += str(recd[i])

    self.info["lat"] = int(("0" * 12) + (S_DataDecText[::-1])[:20], 2) / 6000
    self.info["lon"] = int(("0" * 11) + (S_DataDecText[::-1])[20:41], 2) / 6000

    return ResultDecode.value, int((S_DataDecText[::-1])[41:], 2)


def func_init(self):
    self.read_p = self.mydll.read_p
    self.read_p.argtypes = [POINTER(c_int), POINTER(c_int), c_int * 19]
    self.read_p.restype = c_voidp

    self.generate_gf = self.mydll.generate_gf
    self.generate_gf.argtypes = [POINTER(c_int), POINTER(c_int), c_int * 19, c_int * 274287, c_int * 274287]
    self.generate_gf.restype = c_voidp

    self.gen_poly = self.mydll.gen_poly
    self.gen_poly.argtypes = [c_int, c_int, c_int, POINTER(c_int), POINTER(c_int), POINTER(c_int), c_int * 19, c_int * 274287, c_int * 274287, c_int * 274287]
    self.gen_poly.restype = c_voidp

    self.encode_bch = self.mydll.encode_bch
    self.encode_bch.argtypes = [c_int, c_int, c_int * 524287, c_int * 274287, c_int * 274287]
    self.encode_bch.restype = c_voidp

    self.decode_bch = self.mydll.decode_bch
    self.decode_bch.argtypes = [c_int, c_int, c_int, c_int * 524287, c_int * 274287, c_int * 274287, POINTER(c_int)]
    self.decode_bch.restype = c_voidp

def bch(self):
    m = c_int(7)  #кол-во возможных ошибок
    n = c_int(0)
    mp = pointer(m)
    np = pointer(n)
    arr = c_int * 19
    p = arr()
    self.read_p(mp, np, p)

    m = mp.contents
    n = np.contents
    arr = c_int * 274287
    alpha_to = arr()
    index_of = arr()
    self.generate_gf(mp, np, p, alpha_to, index_of)

    length1 = c_int(85)
    t = c_int(6)
    k = c_int(43)
    g = arr()
    tp = pointer(t)
    dp = pointer(c_int(0))
    kp = pointer(k)
    self.gen_poly(m, n, length1, tp, dp, kp, p, alpha_to, index_of, g)

    bb = arr()
    arr = c_int * 524287
    data = arr()
    for i in range(43):
        data[i] = int(self.inv_data[i])
    self.encode_bch(length1, k, data, g, bb)
    
    encoder = ""
    for i in range(length1.value - k.value):
        encoder += str(bb[i])
    if self.inv_BCH == encoder:
        self.errors = False
    else:
        self.errors = True

    arr = c_int * 524287
    recd = arr()
    for i in range(length1.value - k.value):
        recd[i] = int(self.inv_BCH[i])
    for i in range(k.value):
        recd[i + length1.value - k.value] = data[i]

    ResultDecode = c_int(0)
    ptrResult = pointer(ResultDecode)

    self.decode_bch(t, length1, n, recd, alpha_to, index_of, ptrResult)

    S_DataDecText = ""
    for i in range(k.value - 1, length1.value):
        S_DataDecText += str(recd[i])

    return ResultDecode.value, S_DataDecText[::-1]

def position(self, bits):
    self.info["latitude"] = int(bits[:20], 2) / 6000        # с учетом latt flag
    self.info["longitude"] = int(bits[20:41], 2) / 6000     # с учетом long flag
    self._date_time(int(bits[41:], 2))

def behavior(self, bits):
    self.info["behaviors"] = []
    for i in range(8, 32, 4):
        self.info["behaviors"].append(int(bits[i:i+4], 2))

    minutes = int(bits[32:43], 2)

    h = minutes // 60
    m = minutes % 60

    self.info["date"] = self.dt
    self.info["date"] = self.info["date"].replace(hour=h, minute=m, second=0)
    
    if self.info["date"] > self.dt:
        self.info["date"] -= timedelta(days=1)

    self.info["date"] -= timedelta(minutes=50)

def fill(self, bits):
    if bits[:8] == "11111111":
        self.behavior(bits)
    else:
        self.position(bits)

def _date_time(self, Nmess):
    seconds = 3750

    dat = self.dt - timedelta(seconds=(seconds * Nmess))
    self.info["date"] = datetime.strptime(str(dat)[:19], "%Y-%m-%d %H:%M:%S")
