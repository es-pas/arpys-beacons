# форматы
SISMA_AG = "sisma_ag"       # формат Sisma AG (стратегия 2)
SISMA_HF = "sisma_hf"       # формат Sisma HF (стратегия 3)
SISMA_10RT = "sisma_10rt"   # формат Sisma 10RT (стратегия 3.1)
SISMA_BF = "sisma_bf"       # формат Sisma BF (стратегия 4)

# информация о сообщении
MESSAGE_LENGTH = 144            # длинна сообщения
MESSAGE_PREFIX = "FFFE2F5"      # префикс сообщения
NOT_USING_DATA_LAST_BIT = 56    # неиспользуемые данные сообщения, которые необходимо вырезать
NEW_DATA_BITS_TEMPLATE = "111"  # шаблон флага новых данных