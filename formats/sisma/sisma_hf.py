from ctypes import *
from datetime import datetime, timedelta

from formats.base import Beacon
from formats.sisma.entities import *
from formats.utils.bch import calculate_bch


# префикс, который определяет тип сообщения (позиция / поведение)
	firstPrefixBitHF, lastPrefixBitHF = 0, 8
	behaviorMessagePrefixHF           = "11111111"

# поля, используемые в сообщениях с позицией
FIRST_LATITUDE_BIT_HF, LAST_LATITUDE_BIT_HF = 0, 19
FIRST_LONGITUDE_BIT_HF, LAST_LONGITUDE_BIT_HF = 20, 40
FIRST_NGPS_BIT_HF, LAST_NGPS_BIT_HF = 41, 43
FIRST_BCH_BIT_HF, LAST_BCH_BIT_HF = 43, 85
FIRST_IS_NEW_BIT_HF, LAST_IS_NEW_BIT_HF = 85, 88

# поля, используемые в сообщениях с поведением
	firstBehaviorBitHF, lastBehaviorBitHF       = 8, 32
	behaviorBitsCountHF                         = 4
	firstCurrentHourBitHF, lastCurrentHourBitHF = 32, 43

class SismaHF(Beacon):
    def __init__(self, hex_id: str):
        # self.mydll = CDLL(path)
        # self.func_init()
        self.hex_id = hex_id
        # self.dt = date
        # self.LENGTH = 144
        # self.scale = 16
        # self.byte = 8
        # self.errors=False
        # self.is_new_position = False

    def decode(self, message: str):
        message = MESSAGE_PREFIX + self.hex_id + message

        try:
            message = message.split("*", 1)[0]  # отбрасываем лишние данные в конце строки (если есть)

            bits = bin(int(message, 16))[2:]    # преобразуем шеснадцатиричное сообщение в бинарный вид

            difference = len(bits) - MESSAGE_LENGTH
            if difference > 0:  # елси битов больше, чем должно быть в сообщении, обрезаем
                bits = bits[difference:]
            elif difference < 0:    # елси битов меньше, чем должно быть в сообщении, дополняем нулями
                bits = bits.zfill(MESSAGE_LENGTH)

            bits = bits[NOT_USING_DATA_LAST_BIT:]   # вырезаем неиспользуемые данные
            is_new_data = bits[FIRST_IS_NEW_BIT_HF:LAST_IS_NEW_BIT_HF] == NEW_DATA_BITS_TEMPLATE    # флаг, показывающий, являются ли данные новыми

            calculate_bch(bits[FIRST_LATITUDE_BIT_HF:FIRST_BCH_BIT_HF], bits[FIRST_BCH_BIT_HF:LAST_BCH_BIT_HF])

            self.inv_BCH = (bits[-42:])[::-1]
            self.inv_data = (bits[-85:-42])[::-1] + ("0" * 42)

            self.info = {
                "latitude": 0.0,
                "longitude": 0.0,
                "behaviors": [],
            }

            result, decoded = self.bch()
            if result:
                return None

            self.fill(decoded)

            return self.info
        except:
            return None

    def func_init(self):
        self.read_p = self.mydll.read_p
        self.read_p.argtypes = [POINTER(c_int), POINTER(c_int), c_int * 19]
        self.read_p.restype = c_voidp

        self.generate_gf = self.mydll.generate_gf
        self.generate_gf.argtypes = [POINTER(c_int), POINTER(c_int), c_int * 19, c_int * 274287, c_int * 274287]
        self.generate_gf.restype = c_voidp

        self.gen_poly = self.mydll.gen_poly
        self.gen_poly.argtypes = [c_int, c_int, c_int, POINTER(c_int), POINTER(c_int), POINTER(c_int), c_int * 19, c_int * 274287, c_int * 274287, c_int * 274287]
        self.gen_poly.restype = c_voidp

        self.encode_bch = self.mydll.encode_bch
        self.encode_bch.argtypes = [c_int, c_int, c_int * 524287, c_int * 274287, c_int * 274287]
        self.encode_bch.restype = c_voidp

        self.decode_bch = self.mydll.decode_bch
        self.decode_bch.argtypes = [c_int, c_int, c_int, c_int * 524287, c_int * 274287, c_int * 274287, POINTER(c_int)]
        self.decode_bch.restype = c_voidp

    def bch(self):
        m = c_int(7)  #кол-во возможных ошибок
        n = c_int(0)
        mp = pointer(m)
        np = pointer(n)
        arr = c_int * 19
        p = arr()
        self.read_p(mp, np, p)

        m = mp.contents
        n = np.contents
        arr = c_int * 274287
        alpha_to = arr()
        index_of = arr()
        self.generate_gf(mp, np, p, alpha_to, index_of)

        length1 = c_int(85)
        t = c_int(6)
        k = c_int(43)
        g = arr()
        tp = pointer(t)
        dp = pointer(c_int(0))
        kp = pointer(k)
        self.gen_poly(m, n, length1, tp, dp, kp, p, alpha_to, index_of, g)

        bb = arr()
        arr = c_int * 524287
        data = arr()
        for i in range(43):
            data[i] = int(self.inv_data[i])
        self.encode_bch(length1, k, data, g, bb)
        
        encoder = ""
        for i in range(length1.value - k.value):
            encoder += str(bb[i])
        if self.inv_BCH == encoder:
            self.errors = False
        else:
            self.errors = True

        arr = c_int * 524287
        recd = arr()
        for i in range(length1.value - k.value):
            recd[i] = int(self.inv_BCH[i])
        for i in range(k.value):
            recd[i + length1.value - k.value] = data[i]

        ResultDecode = c_int(0)
        ptrResult = pointer(ResultDecode)

        self.decode_bch(t, length1, n, recd, alpha_to, index_of, ptrResult)

        S_DataDecText = ""
        for i in range(k.value - 1, length1.value):
            S_DataDecText += str(recd[i])

        return ResultDecode.value, S_DataDecText[::-1]

    def position(self, bits):
        self.info["latitude"] = int(bits[:20], 2) / 6000        # с учетом latt flag
        self.info["longitude"] = int(bits[20:41], 2) / 6000     # с учетом long flag
        self._date_time(int(bits[41:], 2))

    def behavior(self, bits):
        self.info["behaviors"] = []
        for i in range(8, 32, 4):
            self.info["behaviors"].append(int(bits[i:i+4], 2))

        minutes = int(bits[32:43], 2)

        h = minutes // 60
        m = minutes % 60

        self.info["date"] = self.dt
        self.info["date"] = self.info["date"].replace(hour=h, minute=m, second=0)
        
        if self.info["date"] > self.dt:
            self.info["date"] -= timedelta(days=1)

        self.info["date"] -= timedelta(minutes=50)

    def fill(self, bits):
        if bits[:8] == "11111111":
            self.behavior(bits)
        else:
            self.position(bits)

    def _date_time(self, Nmess):
        seconds = 3750

        dat = self.dt - timedelta(seconds=(seconds * Nmess))
        self.info["date"] = datetime.strptime(str(dat)[:19], "%Y-%m-%d %H:%M:%S")
